import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const UsingDSG = () => (
  <Layout>
    <h1>Renisa Nurayu Nastiti</h1>
    <p>
      perkenalkan, ini <code>istri</code> saya, kami menikah pada 4 november
      2022. istri saya lahir di <code>Dumai</code>, 3 November 2001.
    </p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export const Head = () => <Seo title="Using DSG" />

export default UsingDSG
