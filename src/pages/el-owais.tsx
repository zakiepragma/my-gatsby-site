// If you don't want to use TypeScript you can delete this file!
import * as React from "react"
import { PageProps, Link, graphql, HeadFC } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

type DataProps = {
  site: {
    buildTime: string
  }
}

const UsingTypescript: React.FC<PageProps<DataProps>> = ({
  data,
  location,
}) => (
  <Layout>
    <h1>
      El Owais
    </h1>
    <p>
      nama <code>El Owais</code> itu bukan nama nabi ataupun sahabat ataupun ulama pada zaman salaf,
      tapi saya memberi nama <code>El Owais</code> karna biar unik dan beda dari yang lain, apalagi di <code>indonesia</code>.
      nama tersebut terinspirasi dari <code>sahabat nabi</code> yang bernama <code>Uwais Al Qorni</code>.
      beliau hidup pada zaman nabi, tapi belum pernah bertemu dengan nabi.
    </p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export const Head: HeadFC<DataProps> = () => <Seo title="Using TypeScript" />

export default UsingTypescript

export const query = graphql`
  {
    site {
      buildTime(formatString: "YYYY-MM-DD hh:mm a z")
    }
  }
`
