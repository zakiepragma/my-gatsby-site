import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const SecondPage = () => (
  <Layout>
    <h1>Al Muzani</h1>
    <p>
      Nama <code>Al Muzani</code> pertama kali kami dengar dari
      <code>Ustadz 'Ariful Bahri</code>, dalam kajian beliau di
      <code>Masjid Syuhada, Air Tiris, Kampar, Riau</code>.
    </p>
    <p>
      pada saat itu beliau membahas kitab <code>Syarhus-Sunnah</code>
      karangan dari <code>Imam Al Muzani</code>. kitab tersebut berisi tentang
      <code>Aqidah-Nya Imam Syafi'i</code>.
    </p>
    <p>
      setelah itu, nama <code>AL Muzani</code> kami dengar juga dari
      <code>Ustadz Khalid Basalamah</code>, waktu itu beliau mencerita-kan
      tentang <code>Iyas Al Muzani</code> seorang hakim pada zaman
      <code>Khalifah Muawwiyah</code>.
    </p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export const Head = () => <Seo title="Page two" />

export default SecondPage
