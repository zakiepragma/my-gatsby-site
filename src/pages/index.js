import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import * as styles from "../components/index.module.css"

const samplePageLinks = [
  { text: "Muhammad Zakie", url: "m-zakie" },
  { text: "Renisa Nurayu Nastiti", url: "renisa-nn" },
  { text: "Al Muzani", url: "al-muzani" },
  { text: "El Owais", url: "el-owais" },
]

const IndexPage = () => (
  <Layout>
    <div className={styles.textCenter}>
      <h1>
        Welcome to <b>My Family!</b>
      </h1>
    </div>
    <ul className={styles.list}>
      {samplePageLinks.map(link => (
        <li key={link.url} className={styles.listItem}>
          <Link className={styles.listItemLink} to={link.url}>
            {link.text} ↗
          </Link>
          <p className={styles.listItemDescription}>{link.description}</p>
        </li>
      ))}
    </ul>
  </Layout>
)

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Home" />

export default IndexPage
